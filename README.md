# Easychallenge

[![CircleCI](https://circleci.com/bb/mauriciobarbosa/easychallenge/tree/master.svg?style=svg)](https://circleci.com/bb/mauriciobarbosa/easychallenge/tree/master)

Aplicativo para cálculo de retorno do investimento.

Autor: Mauricio Barbosa - mauriiciobarbosa@gmail.com


# Overview

A arquitetura utilizada no projeto foi o MVVM com architecture components e Repository Pattern. Foram utilizados os componentes ViewModel e LiveData, para facilitar o gerenciamento do lifecycle e tornar o fluxo de dados mais reativo. O Repository Pattern foi adotado para encapsular o conhecimento da fonte e o tratamento dos dados. Além disso, foi utilizada a organização lógica de feature by package. 

Cada tela é representa por  um fragment. A activity foi utilizada como controlador  de navegação entre as telas. Na tela de resultado foi utilizado o databinding para facilitar na renderização dos dados.

O conceito de Inversão de Controle (IoC) foi introduzido ao projeto para facilitar o gerenciamento de dependências entres os componentes, além de diminuir o acoplamento entre eles.

Por fim, foram adicionados testes locais com o objetivo de validar o comportamento dos métodos utilitários. Por sua vez, os testes integrados têm o propósito de validar o comportamento das telas, além da navegação entre elas. Estes últimos foram implementados utilizando o Robot Pattern.

Obs.: Foi adicionado uma tela a mais para exibir uma mensagem de erro e um botão de "tentar novamente" caso a requisição não seja completada com sucesso.

# Dependencies

* kotlin extensions - Utilizado para reduzir boilerplate code em Fragments e Activities.
* Architecture componentes - Utilizado na arquitetura do app.
* Retrofit - Utilizado para comunicação com a API.
* Koin - Utilizado para injeção de dependências nos componentes da aplicação.
* Canarinho - Utilizado para adicionar máscara aos campos do formulário.
* Shimmer - Adiciona animação na tela de resultado durante o processamento da requisição.
* Espresso - Utilizado para criação dos testes integrados.
* MockWebServer - Utilizado para mockar o server nos testes integrados.
