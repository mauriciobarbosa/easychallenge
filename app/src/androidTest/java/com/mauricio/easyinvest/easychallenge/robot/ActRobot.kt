package com.mauricio.easyinvest.easychallenge.robot

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId

abstract class ActRobot {
    fun fillText(@IdRes viewId: Int, text: String) = onView(withId(viewId)).perform(typeText(text), closeSoftKeyboard())
    fun clickButton(@IdRes buttonId: Int) = onView(withId(buttonId)).perform(click())
}