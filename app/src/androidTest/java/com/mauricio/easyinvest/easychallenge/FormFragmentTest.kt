package com.mauricio.easyinvest.easychallenge

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.mauricio.easyinvest.easychallenge.robot.form.act
import com.mauricio.easyinvest.easychallenge.robot.form.assert
import com.mauricio.easyinvest.easychallenge.ui.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FormFragmentTest {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun whenScreenStartElementsShouldBeVisible() {
        assert {
            textIsVisible("Quanto você gostaria de aplicar? *")
            viewIsVisible(R.id.edit_investment_amount)
            viewIsNotVisible(R.id.txt_error_invested_amount)

            textIsVisible("Qual a data de vencimento do investimento? *")
            viewIsVisible(R.id.edit_maturity_date)
            viewIsNotVisible(R.id.txt_error_maturity_date)

            textIsVisible("Qual o percentual do CDI do investimento? *")
            viewIsVisible(R.id.edit_percentage)
            viewIsNotVisible(R.id.txt_error_percentage)

            viewIsVisible(R.id.btn_simulate)
            simulateButtonIsDisabled()
        }
    }

    @Test
    fun whenFormIsFilledShouldEnableSimulateButton() {
        act {
            fillInvestedAmount("1000")
            fillMaturityDate("31/12/2018")
            fillRate("100")
        }
        assert {
            simulateButtonIsEnabled()
        }
    }

    @Test
    fun whenInvestedAmountInstProvidedShouldShowErrorMessage() {
        act {
            fillInvestedAmount("0")
            fillMaturityDate("31/12/2018")
            fillRate("100")
            simulate()
        }
        assert {
            viewIsVisible(R.id.txt_error_invested_amount)
            viewIsNotVisible(R.id.txt_error_maturity_date)
            viewIsNotVisible(R.id.txt_error_percentage)
        }
    }

    @Test
    fun whenDateIsInvalidShouldShowErrorMessage() {
        act {
            fillInvestedAmount("150")
            fillMaturityDate("31/12/208")
            fillRate("100")
            simulate()
        }
        assert {
            viewIsVisible(R.id.txt_error_maturity_date)
            viewIsNotVisible(R.id.txt_error_invested_amount)
            viewIsNotVisible(R.id.txt_error_percentage)
        }
    }

    @Test
    fun whenDateIsBeforeNowShouldShowErrorMessage() {
        act {
            fillInvestedAmount("150")
            fillMaturityDate("27/10/2018")
            fillRate("100")
            simulate()
        }
        assert {
            viewIsVisible(R.id.txt_error_maturity_date)
            viewIsNotVisible(R.id.txt_error_invested_amount)
            viewIsNotVisible(R.id.txt_error_percentage)
        }
    }

    @Test
    fun whenRateIsntProvidedShouldShowErrorMessage() {
        act {
            fillInvestedAmount("150")
            fillMaturityDate("31/12/2020")
            fillRate("0")
            simulate()
        }
        assert {
            viewIsVisible(R.id.txt_error_percentage)
            viewIsNotVisible(R.id.txt_error_invested_amount)
            viewIsNotVisible(R.id.txt_error_maturity_date)
        }
    }
}