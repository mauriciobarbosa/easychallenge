package com.mauricio.easyinvest.easychallenge.robot.form

import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.robot.ActRobot
import com.mauricio.easyinvest.easychallenge.robot.AssertRobot

open class FormActionRobot : ActRobot() {
    fun fillInvestedAmount(amount: String) = fillText(R.id.edit_investment_amount, amount)
    fun fillMaturityDate(date: String) = fillText(R.id.edit_maturity_date, date)
    fun fillRate(rate: String) = fillText(R.id.edit_percentage, rate)
    fun simulate() = clickButton(R.id.btn_simulate)
}

class FormAssertRobot : AssertRobot() {
    fun simulateButtonIsEnabled() = buttonIsEnabled(R.id.btn_simulate)
    fun simulateButtonIsDisabled() = buttonIsNotEnabled(R.id.btn_simulate)
}

fun act(actObject: FormActionRobot = FormActionRobot(), block: FormActionRobot.() -> Unit) {
    actObject.block()
}

fun assert(assertObject: FormAssertRobot = FormAssertRobot(), block: FormAssertRobot.() -> Unit) {
    assertObject.block()
}