package com.mauricio.easyinvest.easychallenge.core

import com.mauricio.easyinvest.easychallenge.utils.BaseUrlChangingInterceptor
import okhttp3.mockwebserver.MockWebServer
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class ServerTestRule : TestRule {

    val mockWebServer: MockWebServer = MockWebServer()
    private val port = 8080

    override fun apply(base: Statement?, description: Description?): Statement {
        return object : Statement() {
            override fun evaluate() {
                start()
                base?.evaluate()
                stop()
            }
        }
    }

    fun start() {
        mockWebServer.start(port)
        BaseUrlChangingInterceptor.baseUrl = mockWebServer.url("calculator/simulate").url().toString()
    }

    fun stop() {
        mockWebServer.shutdown()
    }
}