package com.mauricio.easyinvest.easychallenge

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.mauricio.easyinvest.easychallenge.core.ServerTestRule
import com.mauricio.easyinvest.easychallenge.robot.navigation.act
import com.mauricio.easyinvest.easychallenge.robot.navigation.arrange
import com.mauricio.easyinvest.easychallenge.robot.navigation.assert
import com.mauricio.easyinvest.easychallenge.ui.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationTest {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    val serverTestRule = ServerTestRule()

    @Test
    fun whenFormIsSubmittedShouldShowResultScreen() {
        arrange(serverTestRule.mockWebServer) {
            mockServerResponseSuccess()
        }
        act {
            fillInvestedAmount("2.000,00")
            fillMaturityDate("31/12/2021")
            fillRate("100")
            simulate()
        }
        assert {
            resultScreenIsVisible()
            amountIs("R$ 2.669,45")
            totalIncomeIs("R$ 569,03")
            investedAmountIs("R$ 2.000,00")
            totalInvestedIs("R$ 2.669,45")
            incomeValueIs("R$ 669,45")
            incomeTaxIs("R$ 100,42[15,00%]")
            netInvestmentIs("R$ 569,03")
            rescueDateIs("31/12/2021")
            periodIs("1160")
            monthlyIncomeIs("0,74%")
            rateIs("100,00%")
            annualProfitabilityIs("33,47%")
            profitabilityRateIs("9,24%")
        }
    }

    @Test
    fun whenClickSimulateAgainShouldReturnToFormScreen() {
        arrange(serverTestRule.mockWebServer) {
            mockServerResponseSuccess()
        }
        act {
            fillInvestedAmount("3.000,00")
            fillMaturityDate("01/01/2025")
            fillRate("100")
            simulate()
            simulateAgain()
        }
        assert {
            amountFieldIsFilledWith("R$ 3.000,00")
            rescueDateFieldIsFilledWith("01/01/2025")
            rateFieldIsFilledWith("100%")
        }
    }

    @Test
    fun whenRequestFailShouldShowErrorScreen() {
        arrange(serverTestRule.mockWebServer) {
            mockServerResponseFail()
        }
        act {
            fillInvestedAmount("3.000,00")
            fillMaturityDate("01/01/2025")
            fillRate("100")
            simulate()
        }
        assert {
            errorScreenIsVisible()
            errorTitleIs("Oooops...")
            errorMessageIs("Algo não saiu como planejado e as\n informações não foram carregadas.")
            tryAgainButtonIsVisible()
        }
    }

    @Test
    fun onErrorScreenWhenClickOnTryAgainShouldDoRequestAgain() {
        arrange(serverTestRule.mockWebServer) {
            mockServerResponseFail()
            mockServerResponseSuccess()
        }
        act {
            fillInvestedAmount("3.000,00")
            fillMaturityDate("01/01/2025")
            fillRate("100")
            simulate()
            tryAgain()
        }
        assert {
            resultScreenIsVisible()
        }
    }
}