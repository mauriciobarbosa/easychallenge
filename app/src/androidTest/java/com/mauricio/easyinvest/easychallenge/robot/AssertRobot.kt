package com.mauricio.easyinvest.easychallenge.robot

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not

abstract class AssertRobot {
    fun textIsVisible(text: String) = onView(withText(text)).check(matches(isDisplayed()))
    fun viewIsVisible(@IdRes id: Int) = onView(withId(id)).check(matches(isDisplayed()))
    fun viewIsNotVisible(@IdRes id: Int) = onView(withId(id)).check(matches(not(isDisplayed())))
    fun buttonIsEnabled(@IdRes id: Int) = onView(withId(id)).check(matches(allOf(isDisplayed(), isEnabled())))
    fun buttonIsNotEnabled(@IdRes id: Int) = onView(withId(id)).check(matches(not(isEnabled())))
    fun viewWithText(@IdRes id: Int, text: String) = onView(withId(id)).check(matches(withText(text)))
}