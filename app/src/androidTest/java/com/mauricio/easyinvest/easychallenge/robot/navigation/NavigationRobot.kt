package com.mauricio.easyinvest.easychallenge.robot.navigation

import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.robot.AssertRobot
import com.mauricio.easyinvest.easychallenge.robot.form.FormActionRobot
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

class NavigationArrangeRobot(private val server: MockWebServer) {

    fun mockServerResponseSuccess() {
        val mockResponse = MockResponse().apply {
            setResponseCode(200)
            setBody(getJson("json/response_success.json"))
        }
        server.enqueue(mockResponse)
    }

    fun mockServerResponseFail() {
        val mockResponse = MockResponse().apply {
            setResponseCode(500)
            setBody(getJson("json/response_empty.json"))
        }
        server.enqueue(mockResponse)
    }

    private fun getJson(path: String): String {
        val context: Context = getApplicationContext()
        val uri = context.assets.open(path)
        return String(uri.readBytes())
    }
}

class NavigationActionRobot : FormActionRobot() {
    fun simulateAgain() = clickButton(R.id.btn_simulate_again)
    fun tryAgain() = clickButton(R.id.btn_retry)
}

class NavigationAssertRobot : AssertRobot() {

    fun amountIs(amount: String) = viewWithText(R.id.txt_final_amount, amount)
    fun totalIncomeIs(totalIncome: String) = viewWithText(R.id.txt_total_income, "Rendimento total de $totalIncome")
    fun investedAmountIs(investedAmount: String) = viewWithText(R.id.txt_initial_amount, investedAmount)
    fun totalInvestedIs(totalInvested: String) = viewWithText(R.id.txt_total_invested, totalInvested)
    fun incomeValueIs(incomeValue: String) = viewWithText(R.id.txt_income_value, incomeValue)
    fun incomeTaxIs(incomeTax: String) = viewWithText(R.id.txt_income_tax, incomeTax)
    fun netInvestmentIs(netInvestment: String) = viewWithText(R.id.txt_net_investment, netInvestment)
    fun rescueDateIs(rescueDate: String) = viewWithText(R.id.txt_rescue_date, rescueDate)
    fun periodIs(period: String) = viewWithText(R.id.txt_period, period)
    fun monthlyIncomeIs(monthlyIncome: String) = viewWithText(R.id.txt_monthly_income, monthlyIncome)
    fun rateIs(rate: String) = viewWithText(R.id.txt_cdi_percentage, rate)
    fun annualProfitabilityIs(annualProfitability: String) =
            viewWithText(R.id.txt_annual_profitability, annualProfitability)

    fun profitabilityRateIs(profitabilityRate: String) =
            viewWithText(R.id.txt_profitability_percentage, profitabilityRate)

    fun amountFieldIsFilledWith(amount: String) = viewWithText(R.id.edit_investment_amount, amount)
    fun rescueDateFieldIsFilledWith(rescueDate: String) = viewWithText(R.id.edit_maturity_date, rescueDate)
    fun rateFieldIsFilledWith(rate: String) = viewWithText(R.id.edit_percentage, rate)

    fun errorTitleIs(title: String) {
        viewIsVisible(R.id.lbl_error_title)
        viewWithText(R.id.lbl_error_title, title)
    }

    fun errorMessageIs(message: String) {
        viewIsVisible(R.id.lbl_error_subtitle)
        viewWithText(R.id.lbl_error_subtitle, message)
    }

    fun tryAgainButtonIsVisible() {
        buttonIsEnabled(R.id.btn_retry)
    }

    fun errorScreenIsVisible() {
        viewIsVisible(R.id.error_view)
        viewIsNotVisible(R.id.result_container)
        viewIsNotVisible(R.id.shimmer_view_container)
    }

    fun resultScreenIsVisible() {
        viewIsVisible(R.id.result_container)
        viewIsNotVisible(R.id.error_view)
        viewIsNotVisible(R.id.shimmer_view_container)
    }
}

fun arrange(server: MockWebServer, block: NavigationArrangeRobot.() -> Unit) {
    NavigationArrangeRobot(server).block()
}

fun act(actObject: NavigationActionRobot = NavigationActionRobot(), block: NavigationActionRobot.() -> Unit) {
    actObject.block()
}

fun assert(assertObject: NavigationAssertRobot = NavigationAssertRobot(), block: NavigationAssertRobot.() -> Unit) {
    assertObject.block()
}