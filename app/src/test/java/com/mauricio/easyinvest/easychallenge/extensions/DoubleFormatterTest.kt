package com.mauricio.easyinvest.easychallenge.extensions

import com.mauricio.easyinvest.easychallenge.utils.moneyToDouble
import org.junit.Assert.assertEquals
import org.junit.Test
import java.lang.NumberFormatException

class DoubleFormatterTest {

    @Test
    fun `when parameter hasn't currency should be parsed correctly`() {
        val value = "150,35"
        val expectedValue = 150.35
        val result = value.moneyToDouble()
        assertEquals(expectedValue, result, 0.5)
    }

    @Test
    fun `when parameter is decimal should be parsed correctly`() {
        val value = "R$ 1.500,88"
        val expectedValue = 1500.88
        val result = value.moneyToDouble()
        assertEquals(expectedValue, result, 0.5)
    }

    @Test
    fun `when parameter isn't decimal should be parsed correctly`() {
        val value = "R$ 1.500"
        val expectedValue = 1500.00
        val result = value.moneyToDouble()
        assertEquals(expectedValue, result, 0.5)
    }

    @Test(expected = NumberFormatException::class)
    fun `when parameter is empty should throw exception`() {
        val value = ""
        value.moneyToDouble()
    }
}