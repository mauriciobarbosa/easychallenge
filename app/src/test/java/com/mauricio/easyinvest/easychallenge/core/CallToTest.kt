package com.mauricio.easyinvest.easychallenge.core

import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class CallToTest<T>(
    private val data: T,
    private val withSuccess: Boolean = true,
    private val shouldThrowException: Boolean = false
) : Call<T> {

    private var isCanceled = false

    override fun enqueue(callback: Callback<T>?) {
        callback?.let {
            if (withSuccess)
                it.onResponse(this, Response.success(data))
            else
                it.onFailure(this, IOException("Error on try retrieve data"))
        }
    }

    override fun isExecuted() = true

    override fun clone(): Call<T> {
        return this.clone()
    }

    override fun isCanceled() = isCanceled

    override fun cancel() {
        isCanceled = true
    }

    override fun execute(): Response<T> {
        return when {
            withSuccess -> Response.success(data)
            shouldThrowException -> throw IOException("Error on try retrieve data")
            else -> {
                val rawResponse = okhttp3.Response.Builder() //
                    .code(500)
                    .protocol(Protocol.HTTP_1_1)
                    .message("{}")
                    .request(Request.Builder().url("http://localhost/").build())
                    .build()
                val body = ResponseBody.create(null, "{}")
                Response.error(body, rawResponse)
            }
        }
    }

    override fun request() = Request.Builder().build()
}