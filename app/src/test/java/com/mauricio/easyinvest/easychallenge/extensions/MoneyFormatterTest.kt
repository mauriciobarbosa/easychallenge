package com.mauricio.easyinvest.easychallenge.extensions

import com.mauricio.easyinvest.easychallenge.utils.formatMoney
import org.junit.Assert
import org.junit.Test
import java.util.Locale

class MoneyFormatterTest {

    @Test
    fun `when value is negative should return money with minus signal`() {
        val value = -150.0
        val expectedResult = "-R$ 150,00"
        val result = value.formatMoney()
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `when locale is informed should return money with correct currency`() {
        val value = 15000.42
        val expectedResult = "$ 15,000.42"
        val result = value.formatMoney(locale = Locale.US)
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `when prefix is informed should return money with prefix`() {
        val value = 42.45
        val expectedResult = "+ R$ 42,45"
        val result = value.formatMoney("+ ")
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `when value is 0 should return money with 0 value`() {
        val value = 0.0
        val expectedResult = "R$ 0,00"
        val result = value.formatMoney()
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `when value has more than 2 digits on second group should return money with 2 digits`() {
        val value = 165.8748909874
        val expectedResult = "R$ 165,87"
        val result = value.formatMoney()
        Assert.assertEquals(expectedResult, result)
    }

    @Test
    fun `when value has only 2 digits on second group should return money with 2 digits`() {
        val value = 165.89
        val expectedResult = "R$ 165,89"
        val result = value.formatMoney()
        Assert.assertEquals(expectedResult, result)
    }
}