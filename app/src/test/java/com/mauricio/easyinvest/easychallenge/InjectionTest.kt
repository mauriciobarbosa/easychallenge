package com.mauricio.easyinvest.easychallenge

import com.mauricio.easyinvest.easychallenge.di.appExecutorsModule
import com.mauricio.easyinvest.easychallenge.di.repositoryModule
import com.mauricio.easyinvest.easychallenge.di.viewModelsModule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.checkModules

class InjectionTest : KoinTest {

    @Test
    fun `should inject components correctly`() {
        checkModules(listOf(appExecutorsModule, repositoryModule, viewModelsModule))
    }
}