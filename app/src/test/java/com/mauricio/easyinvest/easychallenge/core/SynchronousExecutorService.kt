package com.mauricio.easyinvest.easychallenge.core

import java.util.concurrent.AbstractExecutorService
import java.util.concurrent.TimeUnit

class SynchronousExecutorService : AbstractExecutorService() {

    private var shutdown = false

    override fun shutdown() {
        shutdown = true
    }

    override fun shutdownNow(): MutableList<Runnable> {
        shutdown = true
        return mutableListOf()
    }

    override fun isShutdown(): Boolean {
        shutdown = true
        return shutdown
    }

    override fun isTerminated() = shutdown

    override fun execute(command: Runnable?) {
        command?.run()
    }

    override fun awaitTermination(timeout: Long, unit: TimeUnit?) = true
}