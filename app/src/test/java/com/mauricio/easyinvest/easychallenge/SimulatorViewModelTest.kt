package com.mauricio.easyinvest.easychallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mauricio.easyinvest.easychallenge.core.buildRequest
import com.mauricio.easyinvest.easychallenge.core.buildResponse
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorDataSource
import com.mauricio.easyinvest.easychallenge.ui.SimulatorViewModel
import com.nhaarman.mockito_kotlin.KArgumentCaptor
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Captor
import org.mockito.Mock

@RunWith(JUnit4::class)
class SimulatorViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var dataSource: SimulatorDataSource

    @Mock
    lateinit var observer: Observer<InvestmentSimulationResponse>

    @Captor
    lateinit var captor: KArgumentCaptor<InvestmentSimulationRequest>

    private lateinit var viewModel: SimulatorViewModel
    private lateinit var request: InvestmentSimulationRequest

    @Before
    fun setUp() {
        dataSource = mock()
        observer = mock()
        captor = argumentCaptor()
        viewModel = SimulatorViewModel(dataSource)
        request = buildRequest()
    }

    @Test
    fun `when invoke doRequest should call api`() {
        viewModel.result.observeForever(observer)

        whenever(dataSource.doSimulation(any())).thenReturn(buildResponse())

        viewModel.doRequest(request)

        verify(dataSource, times(1)).doSimulation(eq(request))
    }

    @Test
    fun `when call api should pass correct parameters`() {
        viewModel.result.observeForever(observer)

        whenever(dataSource.doSimulation(request)).thenReturn(buildResponse())

        viewModel.doRequest(request)

        verify(dataSource).doSimulation(captor.capture())

        assertEquals(captor.firstValue, request)
    }

    @Test
    fun `when invoke only retry doSimulation shouldn't be called`() {
        viewModel.result.observeForever(observer)

        whenever(dataSource.doSimulation(request)).thenReturn(buildResponse {
            dataSource.doSimulation(request)
        })

        viewModel.retry()

        verify(dataSource, times(0)).doSimulation(eq(request))
    }

    @Test
    fun `when invoke retry after doSimulation api should be called two times at all`() {
        viewModel.result.observeForever(observer)

        whenever(dataSource.doSimulation(request)).thenReturn(buildResponse {
            dataSource.doSimulation(request)
        })

        viewModel.doRequest(request)
        viewModel.retry()

        verify(dataSource, times(2)).doSimulation(eq(request))
    }
}