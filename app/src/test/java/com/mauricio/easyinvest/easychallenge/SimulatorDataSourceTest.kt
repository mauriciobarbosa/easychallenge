package com.mauricio.easyinvest.easychallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mauricio.easyinvest.easychallenge.core.CallToTest
import com.mauricio.easyinvest.easychallenge.core.SynchronousExecutorService
import com.mauricio.easyinvest.easychallenge.core.buildInvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.core.buildRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkState
import com.mauricio.easyinvest.easychallenge.data.source.CalculatorAPI
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorDataSource
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorRemoteDataSource
import com.mauricio.easyinvest.easychallenge.utils.from
import com.nhaarman.mockito_kotlin.KArgumentCaptor
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Captor
import org.mockito.Mock
import retrofit2.Callback

@RunWith(JUnit4::class)
class SimulatorDataSourceTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var api: CalculatorAPI

    @Mock
    lateinit var observerData: Observer<InvestmentSimulationResponse>

    @Mock
    lateinit var observerNetworkState: Observer<NetworkState>

    @Captor
    lateinit var captor: KArgumentCaptor<Callback<InvestmentSimulationResponse>>

    private lateinit var dataSource: SimulatorDataSource
    private lateinit var request: InvestmentSimulationRequest
    private lateinit var params: Map<String, String>
    private lateinit var response: InvestmentSimulationResponse

    @Before
    fun setUp() {
        api = mock()
        observerData = mock()
        observerNetworkState = mock()
        captor = argumentCaptor()

        dataSource = SimulatorRemoteDataSource(api, SynchronousExecutorService())

        request = buildRequest()
        params = mutableMapOf<String, String>() from request
        response = buildInvestmentSimulationResponse()
    }

    @Test(expected = NullPointerException::class)
    fun `when doSimulation is called should call CalculatorApi`() {
        dataSource.doSimulation(request)

        verify(api, times(1)).doSimulation(eq(params))
    }

    @Test
    fun `when doSimulation is successfully should post response`() {
        val call = CallToTest(response, true)

        whenever(api.doSimulation(params)).thenReturn(call)

        val networkResponse = dataSource.doSimulation(request)

        networkResponse.data.observeForever(observerData)
        networkResponse.networkState.observeForever(observerNetworkState)

        verify(observerData, times(1)).onChanged(eq(response))
        verify(observerNetworkState, times(1)).onChanged(eq(NetworkState.SUCCESS))
    }

    @Test
    fun `when doSimulation fail should post only network status equals failed`() {
        val call = CallToTest(response, false)

        whenever(api.doSimulation(params)).thenReturn(call)

        val networkResponse = dataSource.doSimulation(request)

        networkResponse.data.observeForever(observerData)
        networkResponse.networkState.observeForever(observerNetworkState)

        verify(observerData, times(0)).onChanged(any())
        verify(observerNetworkState, times(1)).onChanged(eq(NetworkState.FAILED))
    }

    @Test
    fun `when doSimulation throw exception should post only network status equals failed`() {
        val call = CallToTest(data = response, withSuccess = false, shouldThrowException = true)

        whenever(api.doSimulation(params)).thenReturn(call)

        val networkResponse = dataSource.doSimulation(request)

        networkResponse.data.observeForever(observerData)
        networkResponse.networkState.observeForever(observerNetworkState)

        verify(observerData, times(0)).onChanged(any())
        verify(observerNetworkState, times(1)).onChanged(eq(NetworkState.FAILED))
    }

    @Test
    fun `on retry should call api one more time`() {
        val call = CallToTest(response)

        whenever(api.doSimulation(params)).thenReturn(call)

        val networkResponse = dataSource.doSimulation(request)

        networkResponse.retry()
        networkResponse.retry()

        verify(api, times(3)).doSimulation(eq(params))
    }
}