package com.mauricio.easyinvest.easychallenge.extensions

import com.mauricio.easyinvest.easychallenge.utils.formatPercentage
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class PercentageFormatterTest(private val input: Double, private val expected: String) {

    companion object {

        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<Array<Any>> {
            return listOf(
                    arrayOf(-12.22, "-12,22%"),
                    arrayOf(0, "0,00%"),
                    arrayOf(35.4544390, "35,45%"),
                    arrayOf(35.toDouble(), "35,00%"),
                    arrayOf(35.59, "35,59%")
            )
        }
    }

    @Test
    fun `for each input should return expected result`() {
        val result = input.formatPercentage()
        Assert.assertEquals(expected, result)
    }
}