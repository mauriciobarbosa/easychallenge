package com.mauricio.easyinvest.easychallenge.core

import androidx.lifecycle.MutableLiveData
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkResponse

fun buildRequest(): InvestmentSimulationRequest {
    return InvestmentSimulationRequest(investedAmount = 1000.0,
            rate = 100.0,
            maturityDate = "31/12/2021")
}

fun buildResponse(retry: () -> Unit = {}): NetworkResponse<InvestmentSimulationResponse> {
    return NetworkResponse(data = MutableLiveData(), networkState = MutableLiveData(), retry = retry)
}

fun buildInvestmentSimulationResponse(): InvestmentSimulationResponse {
    return InvestmentSimulationResponse()
}