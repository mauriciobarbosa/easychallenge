package com.mauricio.easyinvest.easychallenge.extensions

import com.mauricio.easyinvest.easychallenge.utils.formatDate
import com.mauricio.easyinvest.easychallenge.utils.isDate
import com.mauricio.easyinvest.easychallenge.utils.toDate
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import java.security.InvalidParameterException
import java.text.ParseException
import java.util.Calendar

@RunWith(Enclosed::class)
class DateUtilsTest {

    class DateFormatter {

        @Test
        fun `when new format isn't provided should convert to default format`() {
            val date = "2019-12-25T00:00:00"
            val expectedResult = "25/12/2019"
            val result = date.formatDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when new format is provided should convert to format provided`() {
            val date = "1998-10-31"
            val expectedResult = "31/10/1998"
            val result = date.formatDate("yyyy-MM-dd")
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when parameter is null should return empty`() {
            val date = null
            val expectedResult = ""
            val result = date.formatDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test(expected = ParseException::class)
        fun `when format doesn't match should throw exception`() {
            "anything".formatDate()
        }
    }

    class DateChecker {

        @Test
        fun `when value is empty should return false`() {
            val value = " "
            val expectedResult = false
            val result = value.isDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when day is invalid should return false`() {
            val value = "31/02/2017"
            val expectedResult = false
            val result = value.isDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when month is invalid should return false`() {
            val value = "28/13/2017"
            val expectedResult = false
            val result = value.isDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when date is valid should return true`() {
            val value = "31/12/2018"
            val expectedResult = true
            val result = value.isDate()
            Assert.assertEquals(expectedResult, result)
        }

        @Test
        fun `when date is incomplete should return false`() {
            val value = "31/12/"
            val expectedResult = false
            val result = value.isDate()
            Assert.assertEquals(expectedResult, result)
        }
    }

    class DateParser {

        @Test(expected = InvalidParameterException::class)
        fun `when value is empty should throw exception`() {
            " ".toDate()
        }

        @Test(expected = InvalidParameterException::class)
        fun `when date is incomplete should throw exception`() {
            "21/10".toDate()
        }

        @Test(expected = InvalidParameterException::class)
        fun `when date is invalid should throw exception`() {
            "31/02/2018".toDate()
        }

        @Test
        fun `when date is valid should return date on default format`() {
            val value = "01/01/2019"
            val expectedResult = buildCalendar(1, Calendar.JANUARY, 2019)

            val result = Calendar.getInstance()
            result.time = value.toDate()

            checkIfCalendarsAreEquals(expectedResult, result)
        }

        @Test
        fun `when format is provided should return date on correct format`() {
            val value = "2019-31-12"
            val expectedResult = buildCalendar(31, Calendar.DECEMBER, 2019)

            val result = Calendar.getInstance()
            result.time = value.toDate("yyyy-dd-MM")

            checkIfCalendarsAreEquals(expectedResult, result)
        }

        @Test(expected = InvalidParameterException::class)
        fun `when format is provided isnt valid should throw exception`() {
            "2019-31-12".toDate("dd/MM/yyyy")
        }

        private fun checkIfCalendarsAreEquals(expectedResult: Calendar, result: Calendar) {
            assertEquals(expectedResult.get(Calendar.DAY_OF_MONTH), result.get(Calendar.DAY_OF_MONTH))
            assertEquals(expectedResult.get(Calendar.MONTH), result.get(Calendar.MONTH))
            assertEquals(expectedResult.get(Calendar.YEAR), result.get(Calendar.YEAR))
        }

        private fun buildCalendar(day: Int, month: Int, year: Int): Calendar {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.DAY_OF_MONTH, day)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            return calendar
        }
    }
}