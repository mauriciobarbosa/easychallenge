package com.mauricio.easyinvest.easychallenge.data

class InvestmentSimulationRequest(
    val investedAmount: Double,
    val rate: Double,
    val maturityDate: String,
    val index: String = "CDI",
    val isTaxFree: Boolean = false
)