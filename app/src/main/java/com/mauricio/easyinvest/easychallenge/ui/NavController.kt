package com.mauricio.easyinvest.easychallenge.ui

interface NavController {

    enum class Page {
        FORM, RESULT
    }

    fun navigateTo(page: Page)
}