package com.mauricio.easyinvest.easychallenge.di

import com.mauricio.easyinvest.easychallenge.data.source.CalculatorAPI
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorDataSource
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorRemoteDataSource
import com.mauricio.easyinvest.easychallenge.ui.SimulatorViewModel
import com.mauricio.easyinvest.easychallenge.utils.BaseUrlChangingInterceptor
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

const val BASE_URL = "githubUrl"
const val NUMBER_OF_THREADS = 3
val appExecutorsModule = module {
    factory<Executor>("networkExecutor") { Executors.newFixedThreadPool(NUMBER_OF_THREADS) }
}

val repositoryModule = module {
    factory {
        Retrofit.Builder()
                .baseUrl(getProperty<String>(BASE_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()
                .create(CalculatorAPI::class.java)
    }
    factory {
        OkHttpClient.Builder().addInterceptor(BaseUrlChangingInterceptor).build()
    }
    single<SimulatorDataSource>("repository") { SimulatorRemoteDataSource(get(), get("networkExecutor")) }
}

val viewModelsModule = module {
    viewModel { SimulatorViewModel(get("repository")) }
}