package com.mauricio.easyinvest.easychallenge.data.source

import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkResponse

interface SimulatorDataSource {
    fun doSimulation(request: InvestmentSimulationRequest): NetworkResponse<InvestmentSimulationResponse>
}