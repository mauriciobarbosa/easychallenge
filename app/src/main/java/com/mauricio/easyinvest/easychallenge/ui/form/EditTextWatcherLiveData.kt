package com.mauricio.easyinvest.easychallenge.ui.form

import androidx.lifecycle.MutableLiveData
import android.widget.EditText

class EditTextWatcherLiveData : MutableLiveData<Boolean>() {

    private val editTextList = mutableListOf<EditText>()

    init {
        value = false
    }

    fun watch(vararg editTexts: EditText) {
        editTextList += editTexts

        editTexts.forEach { editText ->
            editText.setOnKeyListener { _, _, _ ->
                verify()
            }
        }
    }

    private fun verify(): Boolean {
        val newValue = editTextList.all { it.text.toString().isNotEmpty() }

        if (newValue != value)
            value = newValue

        return false
    }
}