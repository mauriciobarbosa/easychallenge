package com.mauricio.easyinvest.easychallenge.data.source

import androidx.lifecycle.MutableLiveData
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkState
import com.mauricio.easyinvest.easychallenge.utils.from
import java.io.IOException
import java.util.concurrent.Executor

class SimulatorRemoteDataSource(private val api: CalculatorAPI, private val networkIO: Executor) : SimulatorDataSource {

    private val networkState = MutableLiveData<NetworkState>()
    private val result = MutableLiveData<InvestmentSimulationResponse>()

    override fun doSimulation(request: InvestmentSimulationRequest): NetworkResponse<InvestmentSimulationResponse> {

        val retry: () -> Unit = { doSimulation(request) }

        networkIO.execute {
            networkState.postValue(NetworkState.RUNNING)

            val params = mutableMapOf<String, String>() from request

            try {
                val response = api.doSimulation(params).execute()

                if (response.isSuccessful) {
                    result.postValue(response.body())
                    networkState.postValue(NetworkState.SUCCESS)
                } else {
                    networkState.postValue(NetworkState.FAILED)
                }
            } catch (exception: IOException) {
                networkState.postValue(NetworkState.FAILED)
            }
        }

        return NetworkResponse(data = result, networkState = networkState, retry = retry)
    }
}