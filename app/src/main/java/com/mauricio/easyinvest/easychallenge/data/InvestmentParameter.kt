package com.mauricio.easyinvest.easychallenge.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class InvestmentParameter(
    @SerializedName("rate") val rate: Double = 0.0,
    @SerializedName("maturityDate") val maturityDate: String = "",
    @SerializedName("investedAmount") val investedAmount: Double = 0.0,
    @SerializedName("maturityTotalDays") val maturityTotalDays: Int = 0
) : Parcelable