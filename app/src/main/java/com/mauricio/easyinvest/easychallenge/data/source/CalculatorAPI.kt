package com.mauricio.easyinvest.easychallenge.data.source

import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface CalculatorAPI {
    @GET("calculator/simulate")
    fun doSimulation(@QueryMap params: Map<String, String>): Call<InvestmentSimulationResponse>
}