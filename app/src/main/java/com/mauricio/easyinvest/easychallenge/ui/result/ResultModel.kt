package com.mauricio.easyinvest.easychallenge.ui.result

import android.content.Context
import android.text.SpannableString
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.utils.formatDate
import com.mauricio.easyinvest.easychallenge.utils.formatMoney
import com.mauricio.easyinvest.easychallenge.utils.formatPercentage
import com.mauricio.easyinvest.easychallenge.utils.formatText

class ResultModel(val data: InvestmentSimulationResponse, private val context: Context) {

    val incomeTax: String
        get() = "${data.taxesAmount.formatMoney()}[${data.taxesRate.formatPercentage()}]"

    val period: String
        get() = data.investmentParameter.maturityTotalDays.toString()

    val netAmount: String
        get() = data.netAmountProfit.formatMoney()

    val netAmountProfit: SpannableString
        get() {
            val totalIncome = context.getString(R.string.total_income, data.netAmountProfit.formatMoney())
            return totalIncome.formatText(context, totalIncome.indexOf("R$"), totalIncome.length)
        }

    val grossAmount: String
        get() = data.grossAmount.formatMoney()

    val investedAmount: String
        get() = data.investmentParameter.investedAmount.formatMoney()

    val grossAmountProfit: String
        get() = data.grossAmountProfit.formatMoney()

    val maturityDate: String
        get() = data.investmentParameter.maturityDate.formatDate()

    val monthlyGrossRateProfit: String
        get() = data.monthlyGrossRateProfit.formatPercentage()

    val annualGrossRateProfit: String
        get() = data.annualGrossRateProfit.formatPercentage()

    val rate: String
        get() = data.investmentParameter.rate.formatPercentage()

    val rateProfit: String
        get() = data.rateProfit.formatPercentage()
}