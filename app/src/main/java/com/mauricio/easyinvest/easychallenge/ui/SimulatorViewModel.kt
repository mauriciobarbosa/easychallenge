package com.mauricio.easyinvest.easychallenge.ui

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationResponse
import com.mauricio.easyinvest.easychallenge.data.NetworkState
import com.mauricio.easyinvest.easychallenge.data.source.SimulatorDataSource
import com.mauricio.easyinvest.easychallenge.ui.form.EditTextWatcherLiveData

class SimulatorViewModel(repository: SimulatorDataSource) : ViewModel() {

    private val request = MutableLiveData<InvestmentSimulationRequest>()
    private val repoResult = map(request, repository::doSimulation)

    val networkState: LiveData<NetworkState> = switchMap(repoResult) { it.networkState }
    val result: LiveData<InvestmentSimulationResponse> = switchMap(repoResult) { it.data }

    val editTextWatcher = EditTextWatcherLiveData()

    fun doRequest(data: InvestmentSimulationRequest) {
        request.value = data
    }

    fun reset(lifecycleOwner: LifecycleOwner) {
        result.removeObservers(lifecycleOwner)
        networkState.removeObservers(lifecycleOwner)
        request.value = null
    }

    fun retry() {
        repoResult.value?.retry?.invoke()
    }
}