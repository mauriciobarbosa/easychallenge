package com.mauricio.easyinvest.easychallenge.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.data.NetworkState
import com.mauricio.easyinvest.easychallenge.databinding.FragResultBinding
import com.mauricio.easyinvest.easychallenge.ui.NavController
import com.mauricio.easyinvest.easychallenge.ui.SimulatorViewModel
import com.mauricio.easyinvest.easychallenge.utils.isVisible
import com.mauricio.easyinvest.easychallenge.utils.observe
import kotlinx.android.synthetic.main.frag_result.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_loading.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ResultFragment : Fragment() {

    private val viewModel by sharedViewModel<SimulatorViewModel>()

    private val lifecycleOwner: LifecycleOwner by lazy {
        activity as? LifecycleOwner ?: throw IllegalStateException("Activity must be a LifecycleOwner")
    }

    private lateinit var resultBinding: FragResultBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        resultBinding = DataBindingUtil.inflate(inflater, R.layout.frag_result, container, false)
        return resultBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val navController = activity as? NavController
            ?: throw IllegalArgumentException("activity must implements NavController")

        btn_simulate_again.setOnClickListener {
            navController.navigateTo(NavController.Page.FORM)
        }

        btn_retry.setOnClickListener {
            viewModel.retry()
        }

        viewModel.networkState.observe(lifecycleOwner, Observer { networkState ->
            result_container.isVisible = networkState == NetworkState.SUCCESS
            shimmer_view_container.isVisible = networkState == NetworkState.RUNNING
            error_view.isVisible = networkState == NetworkState.FAILED
        })

        viewModel.result.observe(lifecycleOwner) { result ->
            activity?.let {
                context -> resultBinding.result = ResultModel(result, context)
            }
        }
    }
}