package com.mauricio.easyinvest.easychallenge.utils

import okhttp3.Interceptor
import okhttp3.Response

object BaseUrlChangingInterceptor : Interceptor {

    var baseUrl: String = ""

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (baseUrl.isNotEmpty()) {
            request = request.newBuilder().url(baseUrl).build()
        }

        return chain.proceed(request)
    }
}