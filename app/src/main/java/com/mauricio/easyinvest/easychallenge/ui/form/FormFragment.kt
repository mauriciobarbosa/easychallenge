package com.mauricio.easyinvest.easychallenge.ui.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import br.com.concrete.canarinho.watcher.MascaraNumericaTextWatcher
import br.com.concrete.canarinho.watcher.ValorMonetarioWatcher
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import com.mauricio.easyinvest.easychallenge.ui.NavController
import com.mauricio.easyinvest.easychallenge.ui.SimulatorViewModel
import com.mauricio.easyinvest.easychallenge.utils.DATE_FORMAT
import com.mauricio.easyinvest.easychallenge.utils.DATE_MASK
import com.mauricio.easyinvest.easychallenge.utils.PERCENTAGE_MASK
import com.mauricio.easyinvest.easychallenge.utils.SERVER_DATE_FORMAT
import com.mauricio.easyinvest.easychallenge.utils.formatDate
import com.mauricio.easyinvest.easychallenge.utils.isDate
import com.mauricio.easyinvest.easychallenge.utils.isVisible
import com.mauricio.easyinvest.easychallenge.utils.moneyToDouble
import com.mauricio.easyinvest.easychallenge.utils.observe
import com.mauricio.easyinvest.easychallenge.utils.toDate
import com.mauricio.easyinvest.easychallenge.utils.unMask
import kotlinx.android.synthetic.main.frag_form.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.Date

class FormFragment : Fragment() {

    private val viewModel by sharedViewModel<SimulatorViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.frag_form, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val navController = activity as? NavController
            ?: throw IllegalArgumentException("activity must implements NavController")

        edit_investment_amount.addTextChangedListener(ValorMonetarioWatcher.Builder()
                .comSimboloReal()
                .comMantemZerosAoLimpar()
                .build())

        edit_maturity_date.addTextChangedListener(MascaraNumericaTextWatcher(DATE_MASK))
        edit_percentage.addTextChangedListener(MascaraNumericaTextWatcher(PERCENTAGE_MASK))

        btn_simulate.setOnClickListener {
            if (isValidForm()) {
                viewModel.doRequest(buildRequest())
                navController.navigateTo(NavController.Page.RESULT)
            }
        }

        viewModel.editTextWatcher.watch(edit_investment_amount, edit_maturity_date, edit_percentage)

        val lifecycleOwner = activity as? LifecycleOwner
            ?: throw IllegalStateException("Activity must be a LifecycleOwner")

        viewModel.editTextWatcher.observe(lifecycleOwner) { allFieldsFilled ->
            btn_simulate.isEnabled = allFieldsFilled
        }
    }

    private fun isValidForm(): Boolean {
        val investedAmountField = edit_investment_amount.text.toString()
        val maturityDateField = edit_maturity_date.text.toString()
        val rateField = edit_percentage.text.toString()

        txt_error_invested_amount.isVisible = investedAmountField.isEmpty() || investedAmountField.moneyToDouble() <= 0
        txt_error_maturity_date.isVisible = with(maturityDateField) { isEmpty() || !isDate() || toDate() <= Date() }
        txt_error_percentage.isVisible = rateField.isEmpty() || rateField.unMask().toDouble() <= 0

        return !txt_error_invested_amount.isVisible &&
                !txt_error_maturity_date.isVisible &&
                !txt_error_percentage.isVisible
    }

    private fun buildRequest(): InvestmentSimulationRequest {
        return InvestmentSimulationRequest(
                investedAmount = edit_investment_amount.text.toString().moneyToDouble(),
                maturityDate = edit_maturity_date.text.toString().formatDate(DATE_FORMAT, SERVER_DATE_FORMAT),
                rate = edit_percentage.text.toString().unMask().toDouble())
    }
}