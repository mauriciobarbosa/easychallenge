package com.mauricio.easyinvest.easychallenge.data

import androidx.lifecycle.LiveData

class NetworkResponse<T>(
    val data: LiveData<T>,
    val networkState: LiveData<NetworkState>,
    val retry: () -> Unit
)