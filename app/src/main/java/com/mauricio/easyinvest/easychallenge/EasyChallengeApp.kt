package com.mauricio.easyinvest.easychallenge

import android.app.Application
import com.mauricio.easyinvest.easychallenge.di.BASE_URL
import com.mauricio.easyinvest.easychallenge.di.appExecutorsModule
import com.mauricio.easyinvest.easychallenge.di.repositoryModule
import com.mauricio.easyinvest.easychallenge.di.viewModelsModule
import org.koin.android.ext.android.startKoin

class EasyChallengeApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(
            androidContext = this,
            modules = listOf(appExecutorsModule, viewModelsModule, repositoryModule),
            extraProperties = mapOf(BASE_URL to BuildConfig.BASE_URL)
        )
    }
}