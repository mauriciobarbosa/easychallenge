package com.mauricio.easyinvest.easychallenge.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.ui.form.FormFragment
import com.mauricio.easyinvest.easychallenge.ui.result.ResultFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), NavController {

    private val viewModel by viewModel<SimulatorViewModel>()

    var page = NavController.Page.FORM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actv_main)

        val fragment = FormFragment()

        supportFragmentManager.beginTransaction().add(R.id.frag_container, fragment).commit()
    }

    override fun navigateTo(page: NavController.Page) {
        this.page = page

        when (page) {
            NavController.Page.FORM -> {
                viewModel.reset(this)
                supportFragmentManager.popBackStack()
            }
            NavController.Page.RESULT -> {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.setCustomAnimations(R.animator.slide_left_enter, R.animator.slide_left_exit,
                                                R.animator.slide_right_enter, R.animator.slide_right_exit)
                transaction.replace(R.id.frag_container, ResultFragment())
                transaction.addToBackStack(null)
                transaction.commit()
            }
        }
    }

    override fun onBackPressed() {
        if (page == NavController.Page.RESULT) {
            navigateTo(NavController.Page.FORM)
        } else {
            finish()
        }
    }
}
