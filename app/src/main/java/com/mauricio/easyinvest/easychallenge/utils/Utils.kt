package com.mauricio.easyinvest.easychallenge.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import android.content.Context
import androidx.core.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import com.mauricio.easyinvest.easychallenge.R
import com.mauricio.easyinvest.easychallenge.data.InvestmentSimulationRequest
import java.security.InvalidParameterException
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

const val DATE_MASK = "##/##/####"
const val PERCENTAGE_MASK = "###%"

const val SERVER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss"
const val DATE_FORMAT = "dd/MM/yyyy"

infix fun MutableMap<String, String>.from(request: InvestmentSimulationRequest): Map<String, String> = with(request) {
    this@from["investedAmount"] = investedAmount.toString()
    this@from["rate"] = rate.toString()
    this@from["maturityDate"] = maturityDate
    this@from["index"] = index
    this@from["isTaxFree"] = isTaxFree.toString()
    return this@from
}

fun <T> LiveData<T>.observe(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, androidx.lifecycle.Observer {
        it?.let(observer)
    })
}

fun Double.formatMoney(prefix: String = "", locale: Locale = Locale("pt", "BR")): String {
    val numberFormater = NumberFormat.getCurrencyInstance(locale)
    var result = numberFormater.format(this)
    if (' ' !in result) {
        val index = result.indexOf('$')
        result = result.substring(0..index) + " " + result.substring(index + 1..result.lastIndex).trim()
    }
    return prefix + result
}

fun Double.formatPercentage(): String {
    return "%.2f%%".format(this).replace('.', ',')
}

fun String?.formatDate(oldFormat: String = SERVER_DATE_FORMAT, newFormat: String = DATE_FORMAT): String {
    if (this == null || this.isEmpty()) return ""
    val oldDateFormatter = SimpleDateFormat(oldFormat, Locale.ENGLISH)
    val newDateFormatter = SimpleDateFormat(newFormat, Locale.ENGLISH)
    return newDateFormatter.format(oldDateFormatter.parse(this))
}

fun String.moneyToDouble(): Double {
    return unMask().toDouble()
}

fun String.unMask(): String {
    return this.replace("R$", "")
            .replace(".", "")
            .replace(",", ".")
            .replace("%", "")
            .trim()
}

fun String.isDate(dateFormat: String = DATE_FORMAT): Boolean {
    val sdf = SimpleDateFormat(dateFormat, Locale.ENGLISH)
    sdf.isLenient = true
    try {
        val date = sdf.parse(this)
        // if a invalid date (ex.: 31/02/2018) was parsed, the result date and the source string will not be equals.
        // The reverse parse below take care of check cases like this.
        return this == sdf.format(date)
    } catch (e: ParseException) {
        return false
    }
}

fun String.toDate(dateFormat: String = DATE_FORMAT): Date {
    if (this.isDate(dateFormat))
        return SimpleDateFormat(dateFormat, Locale.ENGLISH).parse(this)
    throw InvalidParameterException("$this isn't a valid date")
}

fun String?.formatText(context: Context?, start: Int, end: Int): SpannableString {
    if (this == null || context == null) return SpannableString("")

    val spannableString = SpannableString(this)
    spannableString.setSpan(ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimaryDark)), start, end,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

    return spannableString
}

var View.isVisible: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) {
        if (value) this.visibility = View.VISIBLE
        else this.visibility = View.GONE
    }