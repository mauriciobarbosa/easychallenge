package com.mauricio.easyinvest.easychallenge.data

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}